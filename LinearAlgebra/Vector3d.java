//David Tran 1938381

package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;;

	public Vector3d(double xInput, double yInput, double zInput) {
		this.x = xInput;
		this.y = yInput;
		this.z = zInput;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
			return this.y;
		}

	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		double magnitude;
		magnitude = Math.sqrt( (Math.pow(this.x, 2)) + (Math.pow(this.y, 2)) + (Math.pow(this.z, 2)) );
		return magnitude;
	}
	
	public double dotProduct(Vector3d otherVector) {
		double dotProduct;
		dotProduct = ( (this.x * otherVector.getX()) + (this.y * otherVector.getY()) + (this.z * otherVector.getZ()) );
		return dotProduct;
	}
	
	public Vector3d add(Vector3d otherVector){
		double newX;
		double newY;
		double newZ;
		newX = this.x + otherVector.getX();
		newY = this.y + otherVector.getY();
		newZ = this.z + otherVector.getZ();
		Vector3d newVector = new Vector3d(newX, newY, newZ);
		return newVector;
	}

}