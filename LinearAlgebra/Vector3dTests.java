//David Tran 1938381

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGets() {
		Vector3d newVector = new Vector3d (1, 2, 3);
		assertEquals(1, newVector.getX());
		assertEquals(2, newVector.getY());
		assertEquals(3, newVector.getZ());
	}

	@Test
	void testMagnitude() {
		Vector3d newVector = new Vector3d (1, 2, 3);
		assertEquals(Math.sqrt(14), newVector.magnitude() );
	}

	@Test
	void testDotProduct() {
		Vector3d newVectorOne = new Vector3d (1, 2, 3);
		Vector3d newVectorTwo = new Vector3d (3, 2, 1);
		assertEquals(10, newVectorTwo.dotProduct(newVectorOne));
	}

	@Test
	void testAdd() {
		Vector3d newVectorOne = new Vector3d (1, 2, 3);
		Vector3d newVectorTwo = new Vector3d (3, 2, 1);
		assertEquals(4, newVectorTwo.add(newVectorOne).getX());
		assertEquals(4, newVectorTwo.add(newVectorOne).getY());
		assertEquals(4, newVectorTwo.add(newVectorOne).getZ());
		
	}

}